##Minimum App (PHP)

###QuickStart

This sample is the minimum viable code required to make a video call from a web browser into a CafeX Fusion server. The application consists of a basic web page (index.html) with embedded CafeX JavaScript libraries. Session provisioning is handled via an ajax call to a PHP script (session.php) running on a server.

To run this sample, you will need:

1. A CafeX FAS & FCSDK server
2. To edit the two files (index.html and session.php) to point to the IP of your server.
3. To deploy a webserver (e.g. MAMP / WAMP) to host the files and run the php script.

You can read a more detailed article on this implementation [here](https://support.cafex.com/hc/en-us/articles/201777841-What-is-the--code-I-need-to-make-a-video-call-from-a-browser-) or follow the detailed instructions below:


###Instructions

1. Download and install [Git](https://git-scm.com/download/).
2. Download and install [MAMP](https://www.mamp.info/en/downloads/)
3. Start your MAMP server
4. Open up your Git Bash Terminal
5. Change directory into your MAMP/htdocs folder
6. Clone the repository into your MAMP/htdocs directory by running `git clone https://bitbucket.org/rwmorgan/minimal-app-php.git`
(NB. You can fork this repository to you own Bitbucket account first, if you like, to save and share your changes)
7. Change directory into the sample app `cd /minimal-app-php`
8. Open the index.html file in your text editor of choice or you can use vim by running `vim index.html` in the terminal
9. Replace references to `192.168.30.249` to point to your CafeX server IP or FQDN, save the file and close it.
10. Open the session.php file in your text editor or vim by running `vim session.php` in the terminal
11. Replace all references to `192.168.30.249` to point to your own CafeX server IP or FQDN.
12. Within the JSON object, be sure to change the *domain* to your domain and save the file. The default is `webgateway.example.com`
13. Open up the FCSDK sample app `https://your.server.ip.address:8443/csdk-sample` in your web browser and login as user `1001`, password `123`
14. Open (in a new incognito / private window) your MAMP server default page and add the path to your app e.g. `localhost:3000/minimal-app-php`
15. On page load, the app should place a call into user 1001, which you can answer on the sample app side.

